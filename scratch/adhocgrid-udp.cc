#include "ns3/command-line.h"
#include "ns3/config.h"
#include "ns3/uinteger.h"
#include "ns3/double.h"
#include "ns3/string.h"
#include "ns3/log.h"
#include "ns3/yans-wifi-helper.h"
#include "ns3/mobility-helper.h"
#include "ns3/ipv4-address-helper.h"
#include "ns3/yans-wifi-channel.h"
#include "ns3/mobility-model.h"
#include "ns3/olsr-helper.h"
#include "ns3/ipv4-static-routing-helper.h"
#include "ns3/ipv4-list-routing-helper.h"
#include "ns3/internet-stack-helper.h"
#include "ns3/netanim-module.h"
#include "ns3/core-module.h"
#include "ns3/csma-module.h"
#include "ns3/applications-module.h"
#include "ns3/internet-module.h"
using namespace ns3;

NS_LOG_COMPONENT_DEFINE ("AdhocGridUdp");

int main (int argc, char *argv[])
{
    LogComponentEnable ("UdpTraceClient", LOG_LEVEL_INFO);
    LogComponentEnable ("UdpServer", LOG_LEVEL_INFO);
    std::string phyMode ("DsssRate1Mbps");
    double distance = 75;  // m
    uint32_t packetSize = 1000; // bytes
    uint32_t numPackets = 1;
    uint32_t numNodes = 9;  // 3x3
    uint32_t sinkNode = 0;
    uint32_t sourceNode = 4;
    double interval = 1.0; // seconds
    bool tracing = true;
    std::string animFile = "animAdhocGridUDP.xml";

    CommandLine cmd (__FILE__);
    cmd.AddValue ("phyMode", "Wifi Phy mode", phyMode);
    cmd.AddValue ("distance", "distance (m)", distance);
    cmd.AddValue ("packetSize", "size of application packet sent", packetSize);
    cmd.AddValue ("numPackets", "number of packets generated", numPackets);
    cmd.AddValue ("interval", "interval (seconds) between packets", interval);
    cmd.AddValue ("tracing", "turn on ascii and pcap tracing", tracing);
    cmd.AddValue ("numNodes", "number of nodes", numNodes);
    cmd.AddValue ("sinkNode", "Receiver node number", sinkNode);
    cmd.AddValue ("sourceNode", "Sender node number", sourceNode);
    cmd.AddValue ("animFile",  "File Name for Animation Output", animFile);
    cmd.Parse (argc, argv);
    // Convert to time object
    Time interPacketInterval = Seconds (interval); 
      // Fix non-unicast data rate to be the same as that of unicast
    Config::SetDefault ("ns3::WifiRemoteStationManager::NonUnicastMode",
                      StringValue (phyMode));

    NodeContainer nodes;
    nodes.Create (numNodes);
    
    // The below set of helpers will help us to put together the wifi NICs we want
    WifiHelper wifi;
    YansWifiPhyHelper wifiPhy =  YansWifiPhyHelper::Default ();
    wifiPhy.Set ("RxGain", DoubleValue (0) );

    // ns-3 supports RadioTap and Prism tracing extensions for 802.11b
    wifiPhy.SetPcapDataLinkType (WifiPhyHelper::DLT_IEEE802_11_RADIO);

  YansWifiChannelHelper wifiChannel;
  wifiChannel.SetPropagationDelay ("ns3::ConstantSpeedPropagationDelayModel");
  wifiChannel.AddPropagationLoss ("ns3::FriisPropagationLossModel");
  wifiPhy.SetChannel (wifiChannel.Create ());

  // Add an upper mac and disable rate control
  WifiMacHelper wifiMac;
  wifi.SetStandard (WIFI_PHY_STANDARD_80211b);
  wifi.SetRemoteStationManager ("ns3::ConstantRateWifiManager",
                                "DataMode",StringValue (phyMode),
                                "ControlMode",StringValue (phyMode));
  // Set it to adhoc mode
  wifiMac.SetType ("ns3::AdhocWifiMac");
  NetDeviceContainer devices = wifi.Install (wifiPhy, wifiMac, nodes);

  MobilityHelper mobility;
  mobility.SetPositionAllocator ("ns3::GridPositionAllocator",
                                 "MinX", DoubleValue (0.0),
                                 "MinY", DoubleValue (0.0),
                                 "DeltaX", DoubleValue (distance),
                                 "DeltaY", DoubleValue (distance),
                                 "GridWidth", UintegerValue (3),
                                 "LayoutType", StringValue ("RowFirst"));
  mobility.SetMobilityModel ("ns3::ConstantPositionMobilityModel");
  mobility.Install (nodes);

    //ESTO ENVIA LOS PACKETS A MORIR
  InternetStackHelper internet;
  internet.Install(nodes);

   //
 // We've got the "hardware" in place.  Now we need to add IP addresses.
 //

  Address serverAddress;
  NS_LOG_INFO ("Assign IP Addresses.");
  Ipv4AddressHelper ipv4;
  ipv4.SetBase ("10.1.1.0", "255.255.255.0");
  Ipv4InterfaceContainer i = ipv4.Assign (devices);
  serverAddress = Address (i.GetAddress (4));

  //
  // Create one udpServer applications on node one.
  //
   uint16_t port = 4000;
   UdpServerHelper server (port);
   ApplicationContainer apps = server.Install (nodes.Get (4));
   apps.Start (Seconds (1.0));
   apps.Stop (Seconds (10.0));

 // Creamos varios clientes para mandar paquetes udp al servidor.

  for(int j = 0; j < 9; j++){
    if (j!=4){
      uint32_t MaxPacketSize = 1472;  // Back off 20 (IP) + 8 (UDP) bytes from MTU
      UdpTraceClientHelper client (serverAddress, port,"");
      client.SetAttribute ("MaxPacketSize", UintegerValue (MaxPacketSize));
      apps = client.Install (nodes.Get (j));
    }
  }
  apps.Start (Seconds (2.0));
  apps.Stop (Seconds (9.0));
 
  //Generamos los tr y los pcap.
  if (tracing == true){
    AsciiTraceHelper ascii;
    wifiPhy.EnableAsciiAll (ascii.CreateFileStream ("udp-wifi-adhoc.tr"));
    wifiPhy.EnablePcap ("udp-wifi-adhoc-grid", devices);
  }

  Simulator::Stop(Seconds(12.0));
  // Creamos el xml para el NetAnim
  AnimationInterface anim(animFile);
  anim.EnablePacketMetadata();
  NS_LOG_INFO ("Run Simulation.");
  Simulator::Run ();
  Simulator::Destroy ();
  return 0;
}