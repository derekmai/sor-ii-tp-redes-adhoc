# SOR II - TP Redes adhoc

SOR II (Sistemas Operativos y Redes II). Tp sobre Redes Ad-hoc.

## Requerimientos
El objetivo del TP era investigar el ruteo ad-hoc. Se debía explicar el algoritmo de ruteo OLSR, y plantear distintos escenarios mesh para analizar la interacción entre los nodos de la red.

Se deja el informe del TP, donde se plasmó la explicación requerida y en análisis de los distintos escenaros propuestos en la cursada.
